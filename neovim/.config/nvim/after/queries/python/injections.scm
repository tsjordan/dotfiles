;; extends

; Ansible Documentation
((expression_statement
	 (assignment
		 left: (identifier)@_name
		 right: (string
							(string_content)@injection.content)
		 (#any-of? @_name "DOCUMENTATION" "EXAMPLES")
		 (#set! injection.language "yaml")
		 )
	 )
 )
