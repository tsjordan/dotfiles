;; extends

((block_mapping_pair
	 key: (flow_node
					(plain_scalar
						(string_scalar)@key))
	 (#eq? @key "script")
	 value: (block_node
						(block_sequence
							(block_sequence_item
								(flow_node
									(single_quote_scalar)@injection.content)
								(#set! injection.language "bash")
								(#offset! @injection.content 0 1 0 -1)
								)))))

