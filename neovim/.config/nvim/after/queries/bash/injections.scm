;; extends

((command
   name: (command_name) @_name
   argument: (raw_string) @injection.content)
 (#eq? @_name "awk")
 (#set! injection.language "awk"))

((command
   name: (command_name) @_name
   argument: (raw_string) @injection.content)
 (#eq? @_name "jq")
 (#set! injection.language "jq")
 (#offset! @injection.content 0 1 0 -1))

((command
   name: (command_name) @_name
   argument: (raw_string) @injection.content)
 (#eq? @_name "grep")
 (#set! injection.language "regex"))

