syntax match NullCharacter      "\\0" conceal cchar=␀
syntax match StartOfHeading     "" conceal cchar=␁
syntax match StartOfText        "" conceal cchar=␂
syntax match EndOfText          "" conceal cchar=␃
syntax match EndOfTransmission  "" conceal cchar=␄
syntax match Enquiry            "" conceal cchar=␅
syntax match Acknowledge        "" conceal cchar=␆
syntax match Bell               "" conceal cchar=␇
syntax match Backspace          "" conceal cchar=␈
syntax match HorizontalTab      "\t" conceal cchar=␉
syntax match NewLine            "" conceal cchar=␋
syntax match VerticalTab        "" conceal cchar=␌
syntax match FormFeed           "" conceal cchar=␍
syntax match CarriageRet        "" conceal cchar=␎
syntax match ShiftOut           "" conceal cchar=␏
syntax match ShiftIn            "" conceal cchar=␐
syntax match DataLinkEscape     "" conceal cchar=␑
syntax match DeviceControl1     "" conceal cchar=␒
syntax match DeviceControl2     "" conceal cchar=␓
syntax match DeviceControl3     "" conceal cchar=␔
syntax match DeviceControl4     "" conceal cchar=␕
syntax match NegativeAck.       "" conceal cchar=␖
syntax match SynchronousIdle    "" conceal cchar=␗
syntax match EndOfTrans.Blk     "" conceal cchar=␘
syntax match Cancel             "" conceal cchar=␙
syntax match EndOfMedium        "" conceal cchar=␚
syntax match Substitute         "" conceal cchar=␛
syntax match Escape             "" conceal cchar=␜
syntax match FileSeparator      "" conceal cchar=␝
syntax match GroupSeparator     "" conceal cchar=␞
syntax match RecordSeparator    "" conceal cchar=␟
syntax match UnitSeparator      "" conceal cchar=␡
set conceallevel=1
