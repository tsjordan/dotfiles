vim.cmd [[
  aunmenu PopUp
  anoremenu PopUp.Inspect     <cmd>Inspect<CR>
  anoremenu PopUp.InspectTree <cmd>InspectTree<CR>
  amenu PopUp.-LSP-           <NOP>
  anoremenu PopUp.Definition  <cmd>lua vim.lsp.buf.definition()<CR>
  anoremenu PopUp.References  <cmd>lua vim.lsp.buf.references()<CR>
  anoremenu PopUp.Code\ Action  <cmd>lua vim.lsp.buf.code_action()<CR>
  anoremenu PopUp.Hover       <cmd>lua vim.lsp.buf.hover()<CR>
  anoremenu PopUp.Back        <C-T>
  amenu PopUp.-1-             <NOP>
  anoremenu PopUp.Search      *
  ]]
local group = vim.api.nvim_create_augroup("nvim_popupmenu", { clear = true })

vim.api.nvim_create_autocmd("MenuPopup", {
  pattern = "*",
  group = group,
  desc = "Custom PopUp Setup",
  callback = function()
    -- Toggle TreeSitter Menus
    vim.cmd [[
      amenu disable PopUp.Inspect
      amenu disable PopUp.InspectTree
      ]]
    if vim.treesitter.highlighter.active[vim.api.nvim_get_current_buf()] then
      vim.cmd [[
        amenu enable PopUp.Inspect
        amenu enable PopUp.InspectTree
        ]]
    end
    -- Toggle LSP Menus
    vim.cmd [[
      amenu disable PopUp.-LSP-
      amenu disable PopUp.Definition  
      amenu disable PopUp.References  
      amenu disable PopUp.Code\ Action  
      amenu disable PopUp.Hover
      ]]
    if vim.lsp.get_clients({ bufnr = 0})[1] then
    vim.cmd [[
      amenu enable PopUp.-LSP-
      amenu enable PopUp.Definition  
      amenu enable PopUp.References  
      amenu enable PopUp.Code\ Action  
      amenu enable PopUp.Hover
      ]]
    end
  end,
})
  
