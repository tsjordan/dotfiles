return {
  -- NOTE: First, some plugins that don't require any configuration

  -- Git related plugins
  'tpope/vim-rhubarb',

  -- Other utilities
  {
    'junegunn/vim-easy-align',
    config = function()
      vim.keymap.set({ 'n', 'v' },
        'ga',
        '<Plug>(EasyAlign)'
        , { desc = "Easy Align" }
      )
    end
  },
  -- Lazy
  'will133/vim-dirdiff',
  'ayu-theme/ayu-vim',
  { 'git@github.com:tsjordan-eng/blanket.nvim.git', branch = 'cobertura' },
  {
    'mbbill/undotree',
    config = function()
      -- UndoTree --
      vim.keymap.set('n', '<leader>ut', '<cmd>UndotreeToggle<CR>', { desc = 'Undo Tree Toggle' })
      vim.keymap.set('n', '<leader>uu', '<cmd>UndotreeFocus<CR>', { desc = 'Undo Tree Focus' })
    end
  },
  {
    'andrewradev/linediff.vim',
    config = function()
      -- LineDiff
      vim.keymap.set('n', '<leader>ldm', '<cmd>LinediffMerge<CR>', { desc = 'Linediff [M]erge' })
      vim.keymap.set('n', '<leader>ldp', '<cmd>LinediffPick<CR>', { desc = 'Linediff [P]ick' })
      vim.keymap.set('x', '<leader>ldd', ':Linediff<CR>', { desc = 'Line[d]iff' })
    end
  },
  'chrisbra/NrrwRgn',
  'tpope/vim-repeat',
  'tpope/vim-commentary',
  'tpope/vim-surround',
  'tpope/vim-unimpaired',
  {
    'glacambre/firenvim',

    -- Lazy load firenvim
    -- Explanation: https://github.com/folke/lazy.nvim/discussions/463#discussioncomment-4819297
    lazy = not vim.g.started_by_firenvim,
    build = function()
      vim.fn["firenvim#install"](0)
    end,
    config = function()
      vim.g.firenvim_config = {
        globalSettings = { alt = "all" },
        localSettings = {
          [".*"] = {
            cmdline  = "neovim",
            content  = "text",
            priority = 0,
            selector = "textarea",
            takeover = "never"
          }
        }
      }
      vim.api.nvim_create_autocmd({ 'UIEnter' }, {
        callback = function(event)
          local client = vim.api.nvim_get_chan_info(vim.v.event.chan).client
          if client ~= nil and client.name == "Firenvim" then
            vim.o.laststatus = 0
          end
        end
      })

      vim.api.nvim_create_autocmd({ 'BufEnter' }, {
        pattern = "*_tools-scriptrunner*.txt",
        command = "set filetype=ruby"
      })
    end
  },
  'farmergreg/vim-lastplace',

  -- Language-specific
  {
    'MeanderingProgrammer/render-markdown.nvim',
    ft = { "markdown" },
    dependencies = {
      {
        '3rd/image.nvim',
        ft = { "markdown" },
        opts = {
          backend = "kitty",
          processor = "magick_cli", -- or "magick_cli"
          integrations = {
            markdown = {
              enabled = true,
              clear_in_insert_mode = false,
              download_remote_images = true,
              only_render_image_at_cursor = true,
              floating_windows = false,              -- if true, images will be rendered in floating markdown windows
              filetypes = { "markdown", "vimwiki" }, -- markdown extensions (ie. quarto) can go here
            },
          },
        }
      },
    }
  },
  {
    'vim-pandoc/vim-pandoc',
    lazy = true,
    enabled = false,
    ft = { "pandoc", "rst", "textile" },
    cmd = {
      "Pandoc",
      "PandocFolding",
      "PandocHighlight",
      "PandocTemplate",
      "PandocUnhighlight", },
    dependencies = 'vim-pandoc/vim-pandoc-syntax',
    config = function()
      vim.api.nvim_set_var('pandoc#folding#level', 9)
      vim.api.nvim_set_var('pandoc#compiler#arguments', '-f markdown+autolink_bare_uris')
      vim.api.nvim_set_var('pandoc#formatting#extra_equalprg',
        "--atx-headers --from=markdown+pandoc_title_block --to=markdown-simple_tables-multiline_tables-grid_tables+pandoc_title_block-fenced_code_attributes")
      vim.api.nvim_set_var('pandoc#syntax#codeblocks#embeds#use', 1)
      vim.api.nvim_set_var('pandoc#syntax#codeblocks#embeds#langs', { "json", "vim", "cpp", "bash=sh", "mermaid" })
      vim.api.nvim_set_var('pandoc#syntax#conceal#urls', 0)
      vim.api.nvim_set_var('pandoc#spell#enabled', 1)
    end
  },
  'rust-lang/rust.vim',
  'tsjordan-eng/cosmos.vim',
  {
    'tsjordan-eng/cosmos.nvim',
    config = function()
      require('cosmos').setup()
    end,
    dependencies = { 'iwillreku3206/websocket.nvim' }
  },
  'blindfs/vim-regionsyntax',
  'mracos/mermaid.vim',
  'mfussenegger/nvim-ansible',
  {
    's1n7ax/nvim-terminal',
    config = true
  },

  -- database exploration and management
  {
    'kristijanhusak/vim-dadbod-ui',
    dependencies = {
      { 'tpope/vim-dadbod',                     lazy = true },
      { 'kristijanhusak/vim-dadbod-completion', ft = { 'sql', 'mysql', 'plsql' }, lazy = true },
    },
    cmd = {
      'DBUI',
      'DBUIToggle',
      'DBUIAddConnection',
      'DBUIFindBuffer',
    },
    init = function()
      -- Your DBUI configuration
      vim.g.db_ui_use_nerd_fonts = 1
    end,
  },
  {
    'fei6409/log-highlight.nvim',
    config = function()
      require('log-highlight').setup {
        pattern = {
          '/var/log/.*',
          '.*messages%..*',
        },
      }
    end,
  },

  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',
  {
    "L3MON4D3/LuaSnip",
    -- follow latest release.
    version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
    -- install jsregexp (optional!).
    build = "make install_jsregexp"
  },

  -- NOTE: This is where your plugins related to LSP can be installed.
  --  The configuration is done below. Search for lspconfig to find it below.
  {
    'RRethy/vim-illuminate',
    config = function()
      local illuminate = require('illuminate')
      illuminate.configure({
        delay = 0,
        providers = {
          'lsp',
          'treesitter',
        }
      })
      local wrap = true
      vim.keymap.set({ 'n', 'v' }, '<C-n>',
        function()
          illuminate.goto_next_reference(wrap)
        end,
        { desc = 'Go to next reference (Illuminate)' })
      vim.keymap.set({ 'n', 'v' }, '<C-p>',
        function()
          illuminate.goto_prev_reference(wrap)
        end,
        { desc = 'Go to previous reference (Illuminate)' })
      vim.keymap.set({ 'n', 'v' }, 'var', illuminate.textobj_select,
        { desc = 'Select reference (Illuminate)' })
    end
  },
  {
    -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      {
        "williamboman/mason.nvim",
        opts = function(_, opts)
          opts.ensure_installed = opts.ensure_installed or {}
          -- for ansiblels validation
          vim.list_extend(opts.ensure_installed, { "ansible-lint" })
        end,
      },
      'williamboman/mason-lspconfig.nvim',
      {
        url = "https://gitlab.com/schrieveslaach/sonarlint.nvim",
        ft = { "python", "cpp", "java" },
        dependencies = {
          "mfussenegger/nvim-jdtls",
          "williamboman/mason.nvim"
        },
        config = function()
          -- [[ Configure LSP ]]
          --  This function gets run when an LSP connects to a particular buffer.
          local on_attach = function(_, bufnr)
            -- NOTE: Remember that lua is a real programming language, and as such it is possible
            -- to define small helper and utility functions so you don't have to repeat yourself
            -- many times.
            --
            -- In this case, we create a function that lets us more easily define mappings specific
            -- for LSP related items. It sets the mode, buffer and description for us each time.
            local nmap = function(keys, func, desc)
              if desc then
                desc = 'LSP: ' .. desc
              end

              vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
            end

            nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
            nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

            nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
            nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
            nmap('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
            nmap('<leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
            nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
            nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

            -- See `:help K` for why this keymap
            nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
            nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

            -- Lesser used LSP functionality
            nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
            nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
            nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
            nmap('<leader>wl', function()
                print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
              end,
              '[W]orkspace [L]ist Folders')
            nmap('<leader>f', function()
                vim.lsp.buf.format { async = true }
              end,
              '[F]ormat buffer')

            -- Create a command `:Format` local to the LSP buffer
            vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
              vim.lsp.buf.format()
            end, { desc = 'Format current buffer with LSP' })
          end
          local servers = {
            -- gopls = {},
            -- bashls = {},
            clangd = {},
            -- dockerls = {},
            -- pyright = {},
            rubocop = {},
            -- rust_analyzer = {},
            -- solargraph = {},
            -- yamlls = {},
            -- ansiblels = {},
            -- tsserver = {},
            -- html = { filetypes = { 'html', 'twig', 'hbs'} },
            lua_ls = {
              Lua = {
                -- workspace = { checkThirdParty = false },
                -- telemetry = { enable = false },
                runtime = {
                  -- Tell the language server which version of Lua you're using
                  -- (most likely LuaJIT in the case of Neovim)
                  version = 'LuaJIT',
                },
                diagnostics = {
                  -- Get the language server to recognize the `vim` global
                  globals = {
                    'vim',
                    'require'
                  },
                },
                workspace = {
                  -- Make the server aware of Neovim runtime files
                  library = vim.api.nvim_get_runtime_file("", true),
                },
                -- Do not send telemetry data containing a randomized but unique identifier
                telemetry = {
                  enable = false,
                },
              },
            },
          }

          -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
          local capabilities = vim.lsp.protocol.make_client_capabilities()
          capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

          -- Ensure the servers above are installed
          local mason_lspconfig = require 'mason-lspconfig'

          mason_lspconfig.setup {
            ensure_installed = vim.tbl_keys(servers),
            handlers = {
              function(server_name)
                require('lspconfig')[server_name].setup {
                  capabilities = capabilities,
                  on_attach = on_attach,
                  settings = servers[server_name],
                  filetypes = (servers[server_name] or {}).filetypes,
                }
              end
            }
          }

          require("mason-registry")
              .get_package("sonarlint-language-server")
              :get_install_path()

          require("sonarlint").setup({
            server = {
              cmd = {
                vim.fn.expand('$MASON/bin/sonarlint-language-server'),
                -- Ensure that sonarlint-language-server uses stdio channel
                '-stdio',
                '-analyzers',
                -- paths to the analyzers you need, using those for python and java in this example
                vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarpython.jar"),
                vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarcfamily.jar"),
              },
              settings = {
                sonarlint = {
                  pathToCompileCommands = "compile_commands.json",
                  test = "test"
                }
              },
            },
            filetypes = {
              "python",
              "cpp",
              "java"
            }
          })
        end
      },
      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
      {
        -- Autocompletion
        'hrsh7th/nvim-cmp',
        dependencies = {
          -- Snippet Engine & its associated nvim-cmp source
          'L3MON4D3/LuaSnip',
          'saadparwaiz1/cmp_luasnip',

          -- Adds LSP completion capabilities
          'hrsh7th/cmp-nvim-lsp',
          'hrsh7th/cmp-nvim-lsp-signature-help',

          -- Adds a number of user-friendly snippets
          'rafamadriz/friendly-snippets',

          -- Recommendations
          'hrsh7th/cmp-path',
          'hrsh7th/cmp-buffer',
        },
        config = function()
          -- [[ Configure nvim-cmp ]]
          -- See `:help cmp`
          local cmp = require 'cmp'
          local luasnip = require 'luasnip'
          require('luasnip.loaders.from_vscode').lazy_load()
          luasnip.config.setup {}

          ---@diagnostic disable-next-line: missing-fields
          cmp.setup {
            snippet = {
              expand = function(args)
                luasnip.lsp_expand(args.body)
              end,
            },
            mapping = cmp.mapping.preset.insert {
              ['<C-n>'] = cmp.mapping.select_next_item(),
              ['<C-p>'] = cmp.mapping.select_prev_item(),
              ['<C-d>'] = cmp.mapping.scroll_docs(-4),
              ['<C-f>'] = cmp.mapping.scroll_docs(4),
              ['<C-Space>'] = cmp.mapping.complete {},
              ['<CR>'] = cmp.mapping.confirm {
                behavior = cmp.ConfirmBehavior.Replace,
              },
              ['<Tab>'] = cmp.mapping(function(fallback)
                if luasnip.expand_or_locally_jumpable() then
                  luasnip.expand_or_jump()
                else
                  fallback()
                end
              end, { 'i', 's' }),
              ['<S-Tab>'] = cmp.mapping(function(fallback)
                if luasnip.locally_jumpable(-1) then
                  luasnip.jump(-1)
                else
                  fallback()
                end
              end, { 'i', 's' }),
            },
            sources = {
              { name = 'nvim_lsp' },
              { name = 'luasnip' },
              { name = 'nvim_lsp_signature_help' },
              { name = 'path' },
              { name = 'render-markdown' },
              {
                name = 'spell',
                option = {
                  keep_all_entries = false,
                  enable_in_context = function()
                    return true
                  end,
                },
              },
            },
          }
        end
      },
    },
  },
  {
    'WhoIsSethDaniel/toggle-lsp-diagnostics.nvim',
    config = function()
      require('toggle_lsp_diagnostics').init()
      vim.keymap.set('n', '<leader>tl', '<Plug>(toggle-lsp-diag)', { desc = '[T]oggle [L]SP' })
    end,
  },

  -- Useful plugin to show you pending keybinds.
  { 'folke/which-key.nvim',                         opts = {} },
  {
    -- Adds git related signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',
    opts = {
      -- See `:help gitsigns.txt`
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },
      on_attach = function(bufnr)
        vim.keymap.set('n', '<leader>hp', require('gitsigns').preview_hunk,
          { buffer = bufnr, desc = 'Git [H]unk [P]review' })
        vim.keymap.set('n', '<leader>hs', require('gitsigns').stage_hunk, { buffer = bufnr, desc = 'Git [H]unk [S]tage' })
        vim.keymap.set('n', '<leader>hr', require('gitsigns').reset_hunk, { buffer = bufnr, desc = 'Git [H]unk [R]eset' })
        vim.keymap.set('n', '<leader>hu', require('gitsigns').undo_stage_hunk,
          { buffer = bufnr, desc = 'Git [H]unk [U]ndo tage' })
        vim.keymap.set('n', '<leader>wd', require('gitsigns').toggle_word_diff,
          { buffer = bufnr, desc = 'Toggle Git [W]ord [D]iff' })

        -- don't override the built-in and fugitive keymaps
        local gs = package.loaded.gitsigns
        vim.keymap.set({ 'n', 'v' }, ']c', function()
          if vim.wo.diff then return ']c' end
          vim.schedule(function() gs.next_hunk() end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = "Jump to next hunk" })
        vim.keymap.set({ 'n', 'v' }, '[c', function()
          if vim.wo.diff then return '[c' end
          vim.schedule(function() gs.prev_hunk() end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = "Jump to previous hunk" })
      end,
    },
  },

  {
    -- Theme inspired by Atom
    'navarasu/onedark.nvim',
    priority = 1000,
    config = function()
      -- vim.cmd.colorscheme 'onedark'
    end,
  },
  {
    "rebelot/kanagawa.nvim",
    lazy = false,
    priority = 1000,

    config = function()
      require('kanagawa').setup({
        -- colors = {
        -- palette = {
        --   -- change all usages of these colors
        --   sumiInk0 = "#000000",
        --   fujiWhite = "#FFFFFF",
        -- },
        -- theme = {
        --   -- change specific usages for a certain theme, or for all of them
        --   all = {
        --     ui = {
        --       bg = "none",
        --       bg_gutter = "none",
        --     }
        --   }
        -- }
        -- },
      })
      vim.cmd("colorscheme kanagawa-wave")
    end
  },

  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = true,
        component_separators = '|',
        section_separators = '',
      },
      sections = {
        lualine_c = {
          {
            function()
              local node = require 'nvim-treesitter.ts_utils'.get_node_at_cursor()
              while node do
                if node:type() == "function_declaration" or node:type() == "function_definition" then
                  return vim.split(vim.treesitter.get_node_text(node, 0), '\n')[1]
                end
                node = node:parent()
              end
              return ''
            end
            ,
            icon = '󰡱',
            align = 'left'
          }, -- Display current function name
          'filename',
        },
      },
    },
  },

  -- "gc" to comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      -- Fuzzy Finder Algorithm which requires local dependencies to be built.
      -- Only load if `make` is available. Make sure you have the system
      -- requirements installed.
      'nvim-telescope/telescope-symbols.nvim',
      'nvim-telescope/telescope-ui-select.nvim',
      {
        'benfowler/telescope-luasnip.nvim',
        dependencies = { 'L3MON4D3/LuaSnip', }
      },
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        -- NOTE: If you are having trouble with this installation,
        --       refer to the README for telescope-fzf-native for more instructions.
        build = 'make',
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
    },
    config = function()
      -- [[ Configure Telescope ]]
      -- See `:help telescope` and `:help telescope.setup()`
      require('telescope').setup {
        defaults = {
          mappings = {
            i = {
              ['<C-u>'] = false,
              ['<C-d>'] = false,
              ['<C-a>'] = require('telescope.actions').toggle_all,
            },
          },
        },
      }

      -- Enable telescope fzf native, if installed
      pcall(require('telescope').load_extension, 'fzf')
      require("telescope").load_extension("ui-select")
      require("telescope").load_extension("luasnip")


      -- See `:help telescope.builtin`
      vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
      vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
      vim.keymap.set('n', '<leader>/', function()
        -- You can pass additional configuration to telescope to change theme, layout, etc.
        require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
          winblend = 10,
          previewer = false,
        })
      end, { desc = '[/] Fuzzily search in current buffer' })

      vim.keymap.set('n', '<leader>gf', require('telescope.builtin').git_files, { desc = 'Search [G]it [F]iles' })
      vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files, { desc = '[S]earch [F]iles' })
      vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
      vim.keymap.set('n', '<leader>sk', require('telescope.builtin').keymaps, { desc = '[S]earch [K]eymaps' })
      vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
      vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
      vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })
      vim.keymap.set('n', '<leader>sr', require('telescope.builtin').resume, { desc = '[S]earch [R]esume' })
      vim.keymap.set('n', '<leader>sm', require('telescope.builtin').man_pages, { desc = '[S]earch [M]an pages' })
    end
  },
  {
    -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    config = function()
      ---@diagnostic disable-next-line: missing-fields
      require('nvim-treesitter.configs').setup {
        -- Add languages to be installed here that you want installed for treesitter
        ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'ruby', 'bash', 'vimdoc', 'vim', 'jq', 'yaml' },

        -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
        auto_install = false,

        highlight = { enable = true },
        indent = { enable = true },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = '<c-space>',
            node_incremental = '<c-space>',
            scope_incremental = '<c-s>',
            node_decremental = '<M-space>',
          },
        },
        textobjects = {
          select = {
            enable = true,
            lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
            keymaps = {
              -- You can use the capture groups defined in textobjects.scm
              ['aa'] = '@parameter.outer',
              ['ia'] = '@parameter.inner',
              ['af'] = '@function.outer',
              ['if'] = '@function.inner',
              ['ac'] = '@class.outer',
              ['ic'] = '@class.inner',
              ['aC'] = '@conditional.outer',
              ['iC'] = '@conditional.inner',
            },
          },
          move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
              [']m'] = '@function.outer',
              [']]'] = '@class.outer',
            },
            goto_next_end = {
              [']M'] = '@function.outer',
              [']['] = '@class.outer',
            },
            goto_previous_start = {
              ['[m'] = '@function.outer',
              ['[['] = '@class.outer',
            },
            goto_previous_end = {
              ['[M'] = '@function.outer',
              ['[]'] = '@class.outer',
            },
          },
          swap = {
            enable = true,
            swap_next = {
              ['<leader>a'] = '@parameter.inner',
            },
            swap_previous = {
              ['<leader>A'] = '@parameter.inner',
            },
          },
        },
      }
    end,
    build = ':TSUpdate',
    init = function()
      vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
      vim.opt.foldmethod = "expr"
    end
  },
  {
    'stevearc/oil.nvim',
    ---@module 'oil'
    ---@type oil.SetupOpts
    opts = {},
    -- Optional dependencies
    dependencies = { { "echasnovski/mini.icons", opts = {} } },
    -- dependencies = { "nvim-tree/nvim-web-devicons" }, -- use if prefer nvim-web-devicons
  }
}
