-- debug.lua
--
-- Shows how to use the DAP plugin to debug your code.
--
-- Primarily focused on configuring the debugger for Go, but can
-- be extended to other languages as well. That's why it's called
-- kickstart.nvim and not kitchen-sink.nvim ;)

return {
  -- NOTE: Yes, you can install new plugins here!

  'mfussenegger/nvim-dap',
  -- NOTE: And you can specify dependencies as well
  dependencies = {
    -- Creates a beautiful debugger UI
    'rcarriga/nvim-dap-ui',
    'theHamsta/nvim-dap-virtual-text',
    'folke/neodev.nvim',
    'nvim-telescope/telescope-dap.nvim',

    -- async IO dependency
    'nvim-neotest/nvim-nio',

    -- Installs the debug adapters for you
    'williamboman/mason.nvim',
    'jay-babu/mason-nvim-dap.nvim',

    -- Add your own debuggers here
    'leoluz/nvim-dap-go',
    'suketa/nvim-dap-ruby',
    'mfussenegger/nvim-dap-python',

  },
  keys = {
    { '<Shift><F5>', function() require('dap').continue(); end, 'n', desc = "Debug continue/start" },
    '<Leader>db',
    '<Leader>dB',
    '<Leader>dlp',
    '<Leader>ddc',
    '<Leader>ddg',
  },
  cmd = {
    'DapNew',
    'DapShowLog',
    'DapContinue',
    'DapToggleBreakpoint',
    'DapToggleRepl',
    'DapStepOver',
    'DapStepInto',
    'DapStepOut',
    'DapTerminate',
    'DapLoadLaunchJSON',
    'DapRestartFrame',
  },
  config = function()
    local dap = require('dap')
    local dapui = require('dapui')

    require('mason-nvim-dap').setup {
      -- Makes a best effort to setup the various debuggers with
      -- reasonable debug configurations
      automatic_setup = true,
      automatic_installation = false,

      -- see mason-nvim-dap README for more information
      handlers = {},

      ensure_installed = {
        -- Update this to ensure that you have the debuggers for the langs you want
        -- 'delve',
      },
    }

    -- Basic debugging keymaps, feel free to change to your liking!
    -- vim.keymap.set('n', '<Shift><F5>', dap.continue, {desc = "Debug continue"})
    vim.keymap.set('n', '<F10>', dap.step_over, { desc = "Debug Step Over" })
    vim.keymap.set('n', '<F11>', dap.step_into, { desc = "Debug Step Into" })
    vim.keymap.set('n', '<F12>', dap.step_out, { desc = "Debug Step Out" })
    vim.keymap.set('n', '<F3>', dap.terminate, { desc = "Debug Terminate" })
    vim.keymap.set('n', '<F4>', dap.disconnect, { desc = "Debug Disconnect" })
    vim.keymap.set('n', '<Leader>db', dap.toggle_breakpoint, { desc = "Debug toggle_breakpoint" })
    vim.keymap.set('n', '<Leader>dB', dap.set_breakpoint, { desc = "Debug set_breakpoint" })
    vim.keymap.set('n', '<Leader>dlp', function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end,
      { desc = "Debug Set Log Breakpoint" })
    vim.keymap.set('n', '<Leader>dr', function() dap.repl.open() end, { desc = "Debug Open REPL" })
    vim.keymap.set('n', '<Leader>dl', dap.run_last, { desc = "Debug run_last" })
    vim.keymap.set({ 'n', 'v' }, '<Leader>dh', function() require('dap.ui.widgets').hover() end, { desc = "Debug Hover" })
    vim.keymap.set({ 'n', 'v' }, '<Leader>dp', function() require('dap.ui.widgets').preview() end, { desc = "Debug Preview" })
    vim.keymap.set('n', '<Leader>df', function() local widgets = require('dap.ui.widgets') widgets.centered_float(widgets.frames) end, { desc = "Debug Frames Float" })
    vim.keymap.set('n', '<Leader>dv', function() local widgets = require('dap.ui.widgets') widgets.centered_float(widgets.scopes) end, { desc = "Debug Scope Float" })

    -- Dap UI setup
    dapui.setup {}

    -- Toggle to see last session result. Without this, you can't see session output in case of unhandled exception.
    vim.keymap.set('n', '<F7>', dapui.toggle, { desc = 'Debug: See last session result.' })

    dap.listeners.after.event_initialized['dapui_config'] = dapui.open
    dap.listeners.before.event_terminated['dapui_config'] = dapui.close
    dap.listeners.before.event_exited['dapui_config'] = dapui.close

    require('dap-go').setup()
    require('dap-ruby').setup()
    require('dap-python').setup()
    require("nvim-dap-virtual-text").setup {
      only_first_definition = false,
      highlight_new_as_changed = true,
    }

    -- vim.print(dap.configurations)
    -- dap.adapters.sh = dap.adapters.bashdb
    -- dap.configurations.sh = dap.configurations.bashdb
    dap.adapters.sh = {
      type = 'executable',
      command = vim.fn.stdpath("data") .. '/mason/packages/bash-debug-adapter/bash-debug-adapter',
      name = 'bashdb',
    }
    dap.configurations.sh = {
      {
        type = 'sh',
        request = 'launch',
        name = "Launch file with arguments",
        showDebugOutput = true,
        pathBashdb = vim.fn.stdpath("data") .. '/mason/packages/bash-debug-adapter/extension/bashdb_dir/bashdb',
        pathBashdbLib = vim.fn.stdpath("data") .. '/mason/packages/bash-debug-adapter/extension/bashdb_dir',
        trace = true,
        file = "${file}",
        program = "${file}",
        cwd = '${workspaceFolder}',
        pathCat = "cat",
        pathBash = "/bin/bash",
        pathMkfifo = "mkfifo",
        pathPkill = "pkill",
        args = function()
          local args_string = vim.fn.input('Arguments: ')
          return vim.split(args_string, " +")
        end,
        env = {},
        terminalKind = "integrated",
      }
    }

    -- extensions
    require('telescope').load_extension('dap')
    local dap_telescope = require 'telescope'.extensions.dap
    vim.keymap.set('n', '<Leader>ddc', dap_telescope.commands, { desc = "Search Commands" })
    vim.keymap.set('n', '<Leader>ddg', dap_telescope.configurations, { desc = "Search Configurations" })
    vim.keymap.set('n', '<Leader>ddb', dap_telescope.list_breakpoints, { desc = "Search Breakpoint" })
    vim.keymap.set('n', '<Leader>ddv', dap_telescope.variables, { desc = "Search Variables" })
    vim.keymap.set('n', '<Leader>ddf', dap_telescope.frames, { desc = "Search Frames" })
    require('neodev').setup {
      library = { plugins = { "nvim-dap-ui" }, types = true },
    }
  end,
}
