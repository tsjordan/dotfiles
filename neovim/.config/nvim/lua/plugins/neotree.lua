return {
  "nvim-neo-tree/neo-tree.nvim",
  branch = "v3.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",   -- not strictly required, but recommended
    "MunifTanjim/nui.nvim",
    -- "3rd/image.nvim",              -- Optional image support in preview window: See `# Preview Mode` for more information
    "miversen33/netman.nvim",   -- Netman helps connect to remote Directories
  },
  keys = { '-' },
  cmd = { 'Neotree' },
  ft = 'netrw',
  config = function()
    require("neo-tree").setup({
      sources = {
        "filesystem",
        "buffers",
        "git_status",
        "document_symbols",
        "netman.ui.neo-tree",
      },
      filesystem = {
        window = {
          position = "current",
          mappings = {
            -- disable fuzzy finder
            ["/"] = "noop",
            ["f"] = "fuzzy_finder",
          }
        },
        hijack_netrw_behavior = "open_default",
      }
    })
    vim.keymap.set('n', '-', function()
        local reveal_file = vim.fn.expand('%:p')
        if (reveal_file == '') then
          reveal_file = vim.fn.getcwd()
        else
          local f = io.open(reveal_file, "r")
          if (f) then
            f.close(f)
          else
            reveal_file = vim.fn.getcwd()
          end
        end
        require('neo-tree.command').execute({
          toggle = true,
          reveal_file = reveal_file,   -- path to file or folder to reveal
          reveal_force_cwd = true,     -- change cwd without asking
          filesystem = {
            bind_to_cwd = true,    -- true creates a 2-way binding between vim's cwd and neo-tree's root
            cwd_target = {
              sidebar = "tab",     -- sidebar is when position = left or right
              current = "window"   -- current is when position = current
            },
          },
          position = "left",
        })
      end,
      { desc = "Toggle neo-tree at current file or working directory" }
    );
    if vim.bo.ft == 'netrw' then               -- If we stared vim on a directory
      ---@diagnostic disable-next-line: undefined-field
      vim.cmd('Neotree' .. vim.b.netrw_curdir) --netrw_curdir is defined in a netrw buffer
    end
  end
}
