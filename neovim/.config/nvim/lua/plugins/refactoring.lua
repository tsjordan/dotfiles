return {
  "ThePrimeagen/refactoring.nvim",
  keys = {"<leader>re","<leader>rf","<leader>rv","<leader>rI","<leader>ri","<leader>rb","<leader>rbf", },
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-treesitter/nvim-treesitter",
  },
  lazy = true,
  config = function()
    vim.keymap.set("x", "<leader>re", function() require('refactoring').refactor('Extract Function') end,
      { desc = 'Extract function' })
    vim.keymap.set("x", "<leader>rf", function() require('refactoring').refactor('Extract Function To File') end,
      { desc = 'Extract function to file' })
    vim.keymap.set("x", "<leader>rv", function() require('refactoring').refactor('Extract Variable') end,
      { desc = 'Extract variable' })
    vim.keymap.set("n", "<leader>rI", function() require('refactoring').refactor('Inline Function') end,
      { desc = 'Inline function' })
    vim.keymap.set({ "n", "x" }, "<leader>ri", function() require('refactoring').refactor('Inline Variable') end,
      { desc = 'Inline variable' })
    vim.keymap.set("n", "<leader>rb", function() require('refactoring').refactor('Extract Block') end,
      { desc = 'Extract block' })
    vim.keymap.set("n", "<leader>rbf", function() require('refactoring').refactor('Extract Block To File') end,
      { desc = 'Extract block to file' })
    -- Extract block supports only normal mode

    -- load refactoring Telescope extension
    require("telescope").load_extension("refactoring")

    vim.keymap.set(
      { "n", "x" },
      "<leader>rr",
      function() require('telescope').extensions.refactoring.refactors() end,
      { desc = 'Telescope refactoring' }
    )

    -- You can also use below = true here to to change the position of the printf
    -- statement (or set two remaps for either one). This remap must be made in normal mode.
    vim.keymap.set("n", "<leader>rp", function() require('refactoring').debug.printf({ below = false }) end,
      { desc = 'Debug print below cursor' })

    -- Print var
    vim.keymap.set({ "x", "n" }, "<leader>rv", function() require('refactoring').debug.print_var() end,
      { desc = 'Debug Print variable' })
    vim.keymap.set("n", "<leader>rc", function() require('refactoring').debug.cleanup({}) end, { desc = 'Cleanup debug' })
    -- Supports only normal mode

    require("refactoring").setup({
      -- prompt for return type
      prompt_func_return_type = {
        go = true,
        cpp = true,
        c = true,
        java = true,
      },
      -- prompt for function parameters
      prompt_func_param_type = {
        go = true,
        cpp = true,
        c = true,
        java = true,
      },
      -- overriding printf statement for cpp
      printf_statements = {
        -- add a custom printf statement for cpp
        cpp = {
          'PRINT_NOTICE("%s\n");'
        }
      },
      -- overriding printf statement for cpp
      print_var_statements = {
        -- add a custom print var statement for cpp
        cpp = {
          'PRINT_NOTICE("%s %d\n", %s);'
        }
      }

    })
  end,
}
