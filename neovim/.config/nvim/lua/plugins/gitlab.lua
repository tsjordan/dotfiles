return {
  {
    "harrisoncramer/gitlab.nvim",
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "stevearc/dressing.nvim",   -- Recommended but not required. Better UI for pickers.
      "nvim-tree/nvim-web-devicons" -- Recommended but not required. Icons in discussion tree.
    },
    keys = {
      { "glr",  "<cmd>lua require('gitlab').review()<CR>",                                  mode = "n", desc = "Gitlab MR: review" },
      { "gls",  "<cmd>lua require('gitlab').summary()<CR>",                                 mode = "n", desc = "Gitlab MR: summary" },
      { "glS",  "<cmd>lua require('gitlab').start_review()<CR>",                            mode = "n", desc = "Gitlab MR: summary" },
      { "glA",  "<cmd>lua require('gitlab').approve()<CR>",                                 mode = "n", desc = "Gitlab MR: approve" },
      { "glR",  "<cmd>lua require('gitlab').revoke()<CR>",                                  mode = "n", desc = "Gitlab MR: revoke" },
      { "glc",  "<cmd>lua require('gitlab').create_comment()<CR>",                          mode = "n", desc = "Gitlab MR: create_comment" },
      { "glc",  "<cmd>lua require('gitlab').create_multiline_comment()<CR>",                mode = "v", desc = "Gitlab MR: create_multiline_comment" },
      { "glC",  "<cmd>lua require('gitlab').create_comment_suggestion()<CR>",               mode = "v", desc = "Gitlab MR: create_comment_suggestion" },
      { "glO",  "<cmd>lua require('gitlab').create_mr()<CR>",                               mode = "n", desc = "Gitlab MR: create_mr" },
      { "glm",  "<cmd>lua require('gitlab').move_to_discussion_tree_from_diagnostic()<CR>", mode = "n", desc = "Gitlab MR: move_to_discussion_tree_from_diagnostic" },
      { "gln",  "<cmd>lua require('gitlab').create_note()<CR>",                             mode = "n", desc = "Gitlab MR: create_note" },
      { "gld",  "<cmd>lua require('gitlab').toggle_discussions()<CR>",                      mode = "n", desc = "Gitlab MR: toggle_discussions" },
      { "glaa", "<cmd>lua require('gitlab').add_assignee()<CR>",                            mode = "n", desc = "Gitlab MR: add_assignee" },
      { "glad", "<cmd>lua require('gitlab').delete_assignee()<CR>",                         mode = "n", desc = "Gitlab MR: delete_assignee" },
      { "glla", "<cmd>lua require('gitlab').add_label()<CR>",                               mode = "n", desc = "Gitlab MR: add_label" },
      { "glld", "<cmd>lua require('gitlab').delete_label()<CR>",                            mode = "n", desc = "Gitlab MR: delete_label" },
      { "glra", "<cmd>lua require('gitlab').add_reviewer()<CR>",                            mode = "n", desc = "Gitlab MR: add_reviewer" },
      { "glrd", "<cmd>lua require('gitlab').delete_reviewer()<CR>",                         mode = "n", desc = "Gitlab MR: delete_reviewer" },
      { "glp",  "<cmd>lua require('gitlab').pipeline()<CR>",                                mode = "n", desc = "Gitlab MR: pipeline" },
      { "glo",  "<cmd>lua require('gitlab').open_in_browser()<CR>",                         mode = "n", desc = "Gitlab MR: open_in_browser" },
      { "glM",  "<cmd>lua require('gitlab').merge()<CR>",                                   mode = "n", desc = "Gitlab MR: merge" },
    },
    enabled = true,
    build = function() require("gitlab.server").build(true) end, -- Builds the Go binary
    config = function()
      require("gitlab").setup()
    end,
  },

  {
    'shumphrey/fugitive-gitlab.vim',
    config = function()
      vim.g.fugitive_gitlab_domains = { os.getenv("GITLAB_URL") }
      vim.g.gitlab_api_keys = { [os.getenv("GITLAB_URL") or "gitlab.com"] = os.getenv("GITLAB_TOKEN") }
    end,
    dependencies = {
      {
        'tpope/vim-fugitive',
        config = function()
          vim.keymap.set('n',
            '<leader>gs',
            '<cmd>:Git<CR>'
            , { desc = "Git Status" }
          )
          vim.keymap.set('n',
            '<leader>gp',
            '<cmd>:Git push<CR>'
            , { desc = "Git Status" }
          )
        end
      },
    }
  }
}
