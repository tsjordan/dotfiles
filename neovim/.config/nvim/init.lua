-- Set <space> as the leader key
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- [[ Install Lazy package manager ]] --

local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system { 'git', 'clone', '--filter=blob:none', 'https://github.com/folke/lazy.nvim.git', '--branch=stable', lazypath, }
end
vim.opt.rtp:prepend(lazypath)
-- All plugins in plugins subdirectory.
require('lazy').setup('plugins', {})

-- [[ Setting options ]] --

vim.opt.foldenable = false
vim.opt.diffopt:append { 'algorithm:patience' }

-- Set highlight on search
vim.o.hlsearch = true

-- Make line numbers default
vim.wo.number = true
vim.wo.relativenumber = true

-- Enable mouse mode
vim.o.mouse = 'a'

-- default shifts to 4 spaces, Sleuth plugin will refine this
vim.o.shiftwidth = 4
vim.o.tabstop = 4

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

-- NetRW tree style listing with no banner. (Neotree ovrrides netrw though)
vim.g.netrw_liststyle = 3
vim.g.netrw_banner = 0

-- [[ Basic Keymaps ]] --

-- Open Config
vim.keymap.set({ 'n' }, '<leader>ec', ':e $MYVIMRC<CR>', { desc = "Open Configuration" })

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- [[ Highlight on yank ]] --
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- [[ Keymaps ]] --
-- Diagnostic
vim.keymap.set({ 'n' }, ']d', ':lua vim.diagnostic.goto_next({float = false})<CR>', { desc = "Next [D]iagnostic" })
vim.keymap.set({ 'n' }, '[d', ':lua vim.diagnostic.goto_prev({float = false})<CR>', { desc = "Previous [D]iagnostic" })
vim.keymap.set('n', '<leader>d', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>dl', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })
vim.diagnostic.config({ float = { border = "rounded", source = false }, signs = { text = { '', '', ' ', '󰋖' } } })

-- Toggle inlay hints
vim.keymap.set('n', '<leader>ti', function()
    vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
  end,
  { desc = 'Save and execute this [L]ua [F]ile' }
)

-- Quickfix
vim.keymap.set('n', '<leader>qq', '<cmd>cwindow<CR>', { desc = 'Open [Q]uickfix' })
vim.keymap.set('n', '<leader>qc', '<cmd>cclose<CR>', { desc = '[C]lose Quickfix' })
vim.keymap.set('n', '<leader>qo', '<cmd>colder<CR>', { desc = '[Q]uickfix [O]lder' })

-- Terminal escape
vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Escape Terminal' })
-- [[ Filetype specific ]] --

-- Gnuplot SHIFT-K help shortcut
vim.api.nvim_create_autocmd({ 'BufEnter' }, {
  pattern = "*.gpi",
  command = 'set keywordprg=gnuplot\\ -e\\ help\\\\'
})

-- Tarfiles Read-Only
vim.api.nvim_create_autocmd({ 'BufFilePost' }, { pattern = { 'tarfile::*/*', 'tarfile::*' }, command = 'set ro' })

-- source lua file (for nvim lua development)
vim.keymap.set('n', '<leader>lf', function()
    vim.cmd [[
    write
    luafile %
    ]]
  end,
  { desc = 'Save and execute this [L]ua [F]ile' }
)

-- right-click menu
require "menu"
