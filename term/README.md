# Terminal Settings

To get *italics* some systems and TMUX require some tweaking. Run the following
in this directory:

``` {.sh}
tic screen-256color.terminfo
tic -x xterm-256color.terminfo
tic -x tmux-256color.terminfo
```

TMux doesn't seem to set it's term at the right time so you need to set it before you launch it:

```
env TERM=screen-256color tmux
```

Set an alias if you like.

Source: https://gist.github.com/gutoyr/4192af1aced7a1b555df06bd3781a722
