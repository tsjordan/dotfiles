# update this file then run `nix-env -iA nixpkgs.myPackages` to install
{
  packageOverrides = pkgs: with pkgs; {
    myPackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [ delta zoxide bat nodejs ];
    };
  };
}
