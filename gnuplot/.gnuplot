# enable macros
set macros
# search for functions or data files in these directories
set loadpath '~/.config/gnuplot/gnuplot-configs' '~/.config/gnuplot/gnuplot-palettes'
# higher resolution
set samples 1001
# select the desired line style
load 'moreland.pal'
# add macros to select a desired terminal
WXT = "set terminal wxt size 720,480 enhanced font 'Verdana,10' persist"
PNG = "set terminal pngcairo size 720,480 enhanced font 'Verdana,10'; set output 'gnuplot.png'"
SVG = "set terminal svg size 720,480 fname 'Verdana, Helvetica, Arial, sans-serif'; set output 'gnuplot.svg'"
DMB = "set terminal dumb size system('tput cols'),system('tput lines')-3 ansi256"
delta(x) = ( vD = x - old, old = x, vD)
unwrapDelta(x) = ( vD = x - old, old = x, vD>3.14?vD-6.28:(vD<-3.14?vD+6.28:vD))
unwrap(x)      = ( vD = x - old, un = (vD>3.14?x-6.28:(vD<-3.14?x+6.28:x)), old = un, un)
