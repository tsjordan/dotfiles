#!/bin/bash
declare -A osInfo;
osInfo[/etc/redhat-release]=yum
osInfo[/etc/arch-release]=pacman
osInfo[/etc/gentoo-release]=emerge
osInfo[/etc/SuSE-release]=zypp
osInfo[/etc/debian_version]=apt

PKG=""
for f in "${!osInfo[@]}"
do
    if [[ -f $f ]];then
        echo Package manager: ${osInfo[$f]}
	PKG=${osInfo[$f]}
    fi
done

if [[ "$PKG" = apt ]]; then
    ./apt-repos.sh
fi
sudo ${PKG} install neovim git stow tmux -y
# python3-neovim python3-pynvim

echo "Installing dotfiles..."
./gitconfig.sh
stow git
stow starship

# Neovim
stow neovim
nvim --headless "+Lazy! sync" +qa

# TMUX
stow tmux
git submodule init
git submodule update
tmux start-server
tmux new-session -d
sleep 1
~/.tmux/plugins/tpm/scripts/install_plugins.sh
tmux kill-server

# gnuplot
stow gnuplot
echo "installed!"

stow nixpkgs/
