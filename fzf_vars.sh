[ -f ~/.fzf.bash ] && source ~/.fzf.bash
# export FZF_DEFAULT_OPTS="--preview '[[ \$(file --mime {}) =~ binary ]] && \
#                   hexyl {} ||
#                  (bat --style=numbers --color=always {} || \
#                   cat {}) 2> /dev/null | head -120'"
export FZF_DEFAULT_OPTS="--preview '\
  if [[ \$(file --mime {}) =~ image ]] ; then \
    imgcat {} ;
  elif [[ \$(file --mime {}) =~ pdf ]] ; then \
    pdftotext {} - | bat --language=markdown --color=always; \
  elif [[ \$(file --mime {}) =~ docx ]] ; then \
    pandoc {} -t markdown | bat --language=markdown --color=always; \
  elif [[ \$(file --mime {}) =~ binary ]] ; then \
    (hexyl {} || hexdump -C {}) | head -\$FZF_PREVIEW_LINES;\
  else \
    (bat --style=numbers --color=always {} || \
    cat {}) 2> /dev/null | head -\$FZF_PREVIEW_LINES; \
  fi'"
export FZF_DEFAULT_OPTS+="
--preview-window hidden
--bind alt-p:toggle-preview"

export FZF_DEFAULT_COMMAND='fd --type f'

