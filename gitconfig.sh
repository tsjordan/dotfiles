git config --global --add core.editor vim
git config --global --add alias.l 'log --oneline --decorate --all --graph'
git config --global --add color.ui auto
git config --global --add user.name 'Tyler S. Jordan'
git config --global --add diff.word.textconv 'pandoc --to=markdown_mmd' # works with gitattributes' "word"

