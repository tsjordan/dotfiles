Dependencies:

- `stow` to install all the dotfiles
- `ripgrep` for FZF in Neovim
- `python3` for deoplete in Neovim
- `global` for gutentags cscope in Neovim
